export default function floatlabel () {
  const elements = Array.from(document.querySelectorAll('.uiTextInputGroup'))
  if (elements) {
    elements.map((element) => {
      const input = element.querySelector('input')
      let placeholder = input.placeholder
      input.placeholder = ''
      input.addEventListener('focus', () => {
        element.classList.add('uiTextInputGroupFocus')
        input.placeholder = placeholder
      })
      input.addEventListener('blur', () => {
        if (!input.value) {
          element.classList.remove('uiTextInputGroupFocus')
          input.placeholder = ''
        }
      })
    })
  }
}
