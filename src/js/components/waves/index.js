import Waves from 'node-waves'

export default () => {
  const blue = Waves
  blue.attach('.buttonBlue')
  blue.init()
  const blueMedium = Waves
  blueMedium.attach('.buttonBlueMedium')
  blueMedium.init()
  const blueMore = Waves
  blueMore.attach('.buttonBlueMore')
  blueMore.init()

  const white = Waves
  white.attach('.buttonWhite')
  white.init()

  const grey = Waves
  grey.attach('.buttonGrey')
  grey.init()

  const greyLarge = Waves
  greyLarge.attach('.buttonGreyLarge')
  greyLarge.init()

  const simpleText = Waves
  simpleText.attach('.buttonSimpleText')
  simpleText.init()
}
