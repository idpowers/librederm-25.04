import colorSlider from './components/cards/colorSlider'
import floatlabel from './components/floatlabel'
import largeMenu from './components/menu'
import mainSlider from './components/mainSlider'
import modalInit from './components/modal'
import pageSlider from './components/page'
import productSlider from './components/product/'
import search from './components/search'
import waves from './components/waves'

document.addEventListener('DOMContentLoaded', function() {
  mainSlider()
  colorSlider()
  productSlider()
  floatlabel()
  pageSlider()
  modalInit()
  largeMenu()
  search()
  waves()
})
